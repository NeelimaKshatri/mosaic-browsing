def no_of_occurrences(row1, col1, motif, row2, col2, mosaic):
    occurrences = []
    count = 0
    for i in range(row2 - row1 + 1):
        for j in range(col2 - col1 + 1):
            match = True
            for x in range(row1):
                for y in range(col1):
                    if motif[x][y] != 0 and motif[x][y] != mosaic[i + x][j + y]:
                        match = False
                        break
                if not match:
                    break


